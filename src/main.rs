use gtk::prelude::*;
use gtk::{Application, Box, ListBox, Orientation};
use libhandy::prelude::*;
use libhandy::{ActionRow, ApplicationWindow, HeaderBar};

fn main() {
    let application = Application::builder()
        .application_id("com.example.FirstHandyApp")
        .build();

    application.connect_activate(|app| {
        libhandy::init();

        // ActionRows are only available in Handy
        let row = ActionRow::builder()
            .activatable(true)
            .selectable(false)
            .margin(32)
            .title("Click me")
            .build();
        row.connect_activated(|_| {
            eprintln!("Clicked!");
        });

        let list = ListBox::builder().child(&row).build();
        // the content class makes the list look nicer
        list.style_context().add_class("content");

        // Combine the content in a box
        let content = Box::new(Orientation::Vertical, 0);
        // Handy's ApplicationWindow does not include a HeaderBar
        content.add(
            &HeaderBar::builder()
                .show_close_button(true)
                .title("First Handy Program")
                .build(),
        );
        content.add(&list);

        let window = ApplicationWindow::builder()
            .default_width(350)
            .default_height(70)
            // add content to window
            .child(&content)
            .build();
        window.set_application(Some(app));
        window.show_all();
    });

    application.run();
}
